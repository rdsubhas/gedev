## Quickstart

```
# Linux/Mac:
./gradlew test shadowJar
java -jar build/libs/dev-1.0-SNAPSHOT-all.jar "<cityName>"
cat suggestions.csv

# Windows:
gradlew.bat test shadowJar
java -jar build\libs\dev-1.0-SNAPSHOT-all.jar "<cityName>"
type suggestions.csv
```

## IDE Setup

* Recommended: IntelliJ IDEA (Community Edition will work fine)
* Download this repo
* Settings/Preferences > Plugins > Browse Repositories > Install Lombok plugin and Restart
* Open Project -> Choose `build.gradle` -> Let the defaults be as it is
* Settings/Preferences -> Build -> Compiler -> Annotation Processors -> Enable Annotation Processing

Eclipse is no longer a preferred IDE. It tries to be a "platform" with many confusing components (JDT, PDE, Mylyn, APT, EMF, etc). Whereas IntelliJ focuses on only one job, "Being an IDE", and does it well. A programmer just wants a great IDE, not a platform. If you are still using Eclipse, you are on your own to setup this project with Gradle and Lombok.

## Overview

* Built with Lombok annotation processing as the core Java backbone
  * It generates getters/setters/constructors/equals/hashCode/toString/etc for any class
  * All at **compile time** using annotation processing, No runtime impact at all
  * When building the final JAR, Lombok is actually excluded
  * https://projectlombok.org/features/index.html
  * Lombok makes Core Java development as good as Scala. With JDK8 and Lombok, you get pretty much all of Scala's possibilities.

* Uses Retrofit for API access
  * A great library for creating API clients
  * http://square.github.io/retrofit/

* Uses Orika for Bean Mapping
  * In Java (and most statically typed languages), mapping objects from one to another is very verbose
  * Model Mapping is necessary
  * Java has many options: http://modelmapper.org/, http://orika-mapper.github.io/, http://dozer.sourceforge.net/, etc
  * I have used Orika because it does one-time bytecode generation, and is much faster than all others

* Uses Dagger for Dependency Injection
  * A lightweight Dependency Injection tool, compared to Guice/others
  * Does compile-time validation of the object graph
  * Easy to stub during unit testing
  * http://square.github.io/dagger/

* Why so much Annotation-driven?
  * To avoid Plumbing code (getters, setters, builders, calling a http endpoint, etc)
  * Because if you have 5 lines of logic and 100 lines of unnecessary plumbing code to implement that logic, the idea will be lost behind boilerplate.
  * Plumbing code is not real development. Core logic programming is.
  * Lesser stuff to test. If you write lots of plumbing code, you also have to write lots of tests because many trivial things can go wrong. Avoiding plumbing helps to reduce tests, since those libraries are already unit tested.
