package me.subhas.dev;

import dagger.Module;
import dagger.ObjectGraph;
import dagger.Provides;
import ma.glasnost.orika.MapperFacade;
import me.subhas.dev.api.SuggesterApi;
import me.subhas.dev.di.InjectorModule;
import me.subhas.dev.model.GeoPosition;
import me.subhas.dev.model.SuggestHit;
import me.subhas.dev.model.SuggestRow;
import org.junit.Before;
import org.junit.Test;
import retrofit.http.Path;

import javax.inject.Inject;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SuggestAppTest {

    @Inject
    MapperFacade mapperFacade;

    @Inject
    SuggestApp suggestApp;

    @Module(
        includes = InjectorModule.class,
        injects = SuggestAppTest.class,
        overrides = true,
        library = true
    )
    public static class TestModule {
        @Provides
        SuggesterApi overrideSuggesterApi() {
            return new SuggesterApi() {
                @Override
                public List<SuggestHit> suggest(@Path("cityName") String cityName) {
                    return Arrays.asList(createHit());
                }
            };
        }
    }

    @Before
    public void setUp() {
        ObjectGraph.create(new TestModule()).inject(this);
    }

    @Test
    public void shouldMapSuggestHitToRow() {
        SuggestHit hit = createHit();
        SuggestRow row = mapperFacade.map(hit, SuggestRow.class);

        assertThat(row.get_id(), equalTo(hit.getId()));
        assertThat(row.getName(), equalTo(hit.getName()));
        assertThat(row.getType(), equalTo(hit.getType()));
        assertThat(row.getLatitude(), equalTo(hit.getGeoPosition().getLatitude()));
        assertThat(row.getLongitude(), equalTo(hit.getGeoPosition().getLongitude()));
    }

    @Test
    public void shouldWriteCsv() throws IOException {
        StringWriter stringWriter = new StringWriter();
        suggestApp.process("test", stringWriter);

        String[] rows = stringWriter.getBuffer().toString().split("\r\n");
        assertThat(rows.length, equalTo(2));

        String[] columns = rows[1].split(",");
        assertThat(columns[0], equalTo("1234"));
        assertThat(columns[1], equalTo("testName"));
        assertThat(columns[2], equalTo("testType"));
        assertThat(columns[3], equalTo("1.0"));
        assertThat(columns[4], equalTo("2.0"));
    }

    static SuggestHit createHit() {
        return SuggestHit.builder()
            .id(1234L)
            .name("testName")
            .type("testType")
            .geoPosition(new GeoPosition(1.0, 2.0))
            .build();
    }

}
