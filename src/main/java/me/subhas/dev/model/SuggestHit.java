package me.subhas.dev.model;

import com.google.gson.annotations.SerializedName;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = {"id"})
public class SuggestHit {

    @SerializedName("_id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("fullName")
    private String fullName;

    @SerializedName("iata_airport_code")
    private String iataAirportCode;

    @SerializedName("type")
    private String type;

    @SerializedName("country")
    private String country;

    @SerializedName("geo_position")
    private GeoPosition geoPosition;

    @SerializedName("location_id")
    private Long locationId;

    @SerializedName("inEurope")
    private Boolean inEurope;

    @SerializedName("countryCode")
    private String countryCode;

    @SerializedName("coreCountry")
    private Boolean coreCountry;

}
