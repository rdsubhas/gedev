package me.subhas.dev.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SuggestRow {

    private Long _id;
    private String name;
    private String type;
    private Double latitude;
    private Double longitude;

}
