package me.subhas.dev.api;

import me.subhas.dev.model.SuggestHit;
import retrofit.http.GET;
import retrofit.http.Path;

import java.util.List;

public interface SuggesterApi {

    @GET("/api/v2/position/suggest/en/{cityName}")
    List<SuggestHit> suggest(@Path("cityName") String cityName);

}
