package me.subhas.dev.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Module;
import dagger.Provides;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import me.subhas.dev.SuggestApp;
import me.subhas.dev.api.SuggesterApi;
import me.subhas.dev.model.SuggestHit;
import me.subhas.dev.model.SuggestRow;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import javax.inject.Singleton;

@Module(injects = SuggestApp.class)
public class InjectorModule {

    @Provides @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides @Singleton
    RestAdapter provideRestAdapter(Gson gson) {
        return new RestAdapter.Builder()
            .setConverter(new GsonConverter(gson))
            .setEndpoint("http://api.goeuro.com")
            .build();
    }

    @Provides @Singleton
    SuggesterApi provideSuggesterApi(RestAdapter restAdapter) {
        return restAdapter.create(SuggesterApi.class);
    }

    @Provides @Singleton
    MapperFacade provideMapperFacade() {
        return createMapperFactory().getMapperFacade();
    }

    MapperFactory createMapperFactory() {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(SuggestHit.class, SuggestRow.class)
            .field("id", "_id")
            .field("name", "name")
            .field("type", "type")
            .field("geoPosition.latitude", "latitude")
            .field("geoPosition.longitude", "longitude")
            .byDefault()
            .register();
        return factory;
    }

}
