package me.subhas.dev;

import dagger.ObjectGraph;
import me.subhas.dev.di.InjectorModule;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Main {

    public static void main(String... args) throws IOException {
        if (args.length == 0 || args[0].length() == 0) {
            System.err.println("USAGE: java -jar <path/to.jar> CITY_NAME");
            return;
        }

        String cityName = args[0];
        Writer writer = new FileWriter("suggestions.csv");
        ObjectGraph objectGraph = ObjectGraph.create(new InjectorModule());
        SuggestApp suggestApp = objectGraph.get(SuggestApp.class);
        suggestApp.process(cityName, writer);
    }

}
