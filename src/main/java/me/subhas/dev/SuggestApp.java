package me.subhas.dev;

import lombok.Cleanup;
import ma.glasnost.orika.MapperFacade;
import me.subhas.dev.api.SuggesterApi;
import me.subhas.dev.model.SuggestHit;
import me.subhas.dev.model.SuggestRow;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class SuggestApp {

    final String[] HEADERS = new String[] {
        "_id", "name", "type", "latitude", "longitude"
    };

    @Inject
    SuggesterApi suggesterApi;

    @Inject
    MapperFacade mapperFacade;

    public void process(String cityName, Writer writer) throws IOException {
        List<SuggestHit> suggestHits = suggesterApi.suggest(cityName);
        List<SuggestRow> suggestRows = mapperFacade.mapAsList(suggestHits, SuggestRow.class);

        @Cleanup CsvBeanWriter beanWriter = new CsvBeanWriter(writer, CsvPreference.STANDARD_PREFERENCE);
        beanWriter.writeHeader(HEADERS);
        for (SuggestRow row: suggestRows) {
            beanWriter.write(row, HEADERS);
        }
    }

}
